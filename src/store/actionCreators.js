//ACTION CONSTANTS
export const ADD_DEPENDENCY = "ADD_DEPENDENCY";
export const DELETE_DEPENDENCY = "DELETE_DEPENDENCY";
export const DELETE_CHIP = "DELETE_CHIP";
export const FORM_SUBMITTED = "FORM_SUBMITTED";
export const STEP_CHANGE = "STEP_CHANGE";
export const TOGGLE_CUSTOMURL_CHECK = "TOGGLE_CUSTOMURL_CHECK";
export const HANDLE_TEXTFIELD_CHANGE = "HANDLE_TEXTFIELD_CHANGE";
export const HANDLE_PROJECT_NAME_CHANGE = "HANDLE_PROJECT_NAME_CHANGE";
export const HANDLE_PROJECT_NAME_SUBMIT_BUTTON_CLICK =
  "HANDLE_PROJECT_NAME_SUBMIT_BUTTON_CLICK";
export const UPDATE_CONTAINER_NAMES = "UPDATE_CONTAINER_NAMES";
export const ADD_CONTAINERS = "ADD_CONTAINERS";
export const TOGGLE_INIT_CONTAINER = "TOGGLE_INIT_CONTAINER";
export const REMOVE_INIT_CONTAINER = "REMOVE_INIT_CONTAINER";
export const ADD_INIT_CONTAINER = "ADD_INIT_CONTAINER";
export const ERROR_MSG = "ERROR_MSG";

const initialFormState = {
  image: "",
  tag: "",
  ports: "",
  env_variables: "",
  command: "",
  dependencies: [],
  custom_url: "",
  custom_url_check: false
};

export const handleAddChip = (name, value) => {
  return { type: ADD_DEPENDENCY, payload: { name, value } };
};
export const handleDeleteChip = (name, value, index) => {
  return { type: DELETE_CHIP, payload: { name, value, index } };
};
export const handleRemoveDependency = (name, value, index) => {
  return { type: DELETE_DEPENDENCY, payload: { name, value, index } };
};
export const handleTextFieldChange = (name, value, field) => {
  return { type: HANDLE_TEXTFIELD_CHANGE, payload: { name, value, field } };
};
export const handleSubmit = () => {
  return { type: FORM_SUBMITTED };
};

export const handleProjectNameChange = e => {
  return {
    type: HANDLE_PROJECT_NAME_CHANGE,
    payload: {
      projectName: e.target.value
    }
  };
};

export const handleSubmitProjectName = () => {
  return {
    type: HANDLE_PROJECT_NAME_SUBMIT_BUTTON_CLICK
  };
};

export const updateContainerNames = (index, value) => {
  return {
    type: UPDATE_CONTAINER_NAMES,
    payload: {
      index,
      value
    }
  };
};

export const handleAddContainerButton = () => {
  return {
    type: ADD_CONTAINERS
  };
};
export const stepChange = (step, containers) => {
  let forms = {};
  containers.forEach((item, index) => {
    forms[item] = initialFormState;
  });
  return { type: STEP_CHANGE, step, forms };
};
export const toggleCheckCustomUrl = (name, checked) => {
  return { type: TOGGLE_CUSTOMURL_CHECK, payload: { name, checked } };
};
export const updateInitContainer = (e, checked) => {
  if (checked) {
    return dispatch => {
      dispatch(addInitContainer());
      dispatch(handleToggleInitContainer());
    };
  }
  return dispatch => {
    dispatch(removeInitContainer());
    dispatch(handleToggleInitContainer());
  };
};
export const addInitContainer = () => {
  return {
    type: ADD_INIT_CONTAINER
  };
};
export const handleToggleInitContainer = () => {
  return {
    type: TOGGLE_INIT_CONTAINER
  };
};

export const removeInitContainer = () => {
  return {
    type: REMOVE_INIT_CONTAINER
  };
};

export const showErrorMsg = error => {
  return {
    type: ERROR_MSG,
    payload: {
      errorMsg: error
    }
  };
};

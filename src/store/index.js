import { createStore, applyMiddleware, compose } from "redux";
import { reducer } from "./../reducers/rootReducer";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

let store;

if (process.env.NODE_ENV !== "production") {
  store = createStore(
    reducer,
    /* preloadedState, */ composeWithDevTools(),
    applyMiddleware(thunk),
  );
} else {
  store = createStore(reducer);
}
export default store;

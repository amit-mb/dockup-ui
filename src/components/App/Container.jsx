import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import Card from "@material-ui/core/Card";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/Input";
import Input from "@material-ui/core/Input";
import ChipInput from "material-ui-chip-input";
import Divider from "@material-ui/core/Divider";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

const styles = {
  card: {
    minWidth: 275,
    padding: 20,
  },
  expansionSummary: {
    fontSize: 14,
    fontWeight: 400,
  },
  noBorder: {
    border: "none",
    boxShadow: "none",
  },
  noPadding: {
    padding: 0,
  },
  noPaddingdisplayBlock: {
    padding: 0,
    display: "block",
  },
};

const ContainerForm = props => {
  const {
    name,
    image,
    tag,
    env_variables,
    dependencies,
    ports,
    command,
    custom_url_check,
    custom_url,
    handleRemoveDependency,
    handleAddChip,
    handleDeleteChip,
    toggleCheckCustomUrl,
    handleTextFieldChange,
  } = props;
  return (
    <Card className={props.classes.card}>
      <Typography variant="headline" gutterBottom>
        {name}
      </Typography>
      <Divider />
      <FormControl required={true}>
        <InputLabel htmlFor="image_url">Image</InputLabel>
        <Input
          type="text"
          name="image"
          value={image}
          onChange={e => handleTextFieldChange(name, e.target.value, "image")}
        />
      </FormControl>
      <FormControl required={true}>
        <InputLabel htmlFor="tag">Tag</InputLabel>
        <Input
          type="text"
          name="image"
          value={tag}
          onChange={e => handleTextFieldChange(name, e.target.value, "tag")}
        />
      </FormControl>
      <FormControl required={true}>
        <InputLabel htmlFor="ports">Ports To Be Exposed</InputLabel>
        <Input
          type="text"
          name="ports"
          value={ports}
          onChange={e => handleTextFieldChange(name, e.target.value, "ports")}
        />
      </FormControl>
      <FormControl required={true}>
        <InputLabel htmlFor="env_variables">Environment Variables</InputLabel>
        <TextField
          id="env_variables"
          name="env_variables"
          hinttext="Enter you Environment variables"
          helpertext="Enter key-value pairs in form KEY='VALUE'"
          placeholder="AWS_SECRET='secret'"
          multiline={true}
          rows={10}
          value={env_variables}
          onChange={e =>
            handleTextFieldChange(name, e.target.value, "env_variables")
          }
        />
      </FormControl>
      <div>
        <FormControlLabel
          control={
            <Checkbox
              checked={custom_url_check}
              value={custom_url_check.toString()}
              onChange={() => toggleCheckCustomUrl(name, !custom_url_check)}
              color="primary"
            />
          }
          label="Need a custom URL?"
        />
        {custom_url_check ? (
          <TextField
            name="custom-url"
            hinttext="Enter a custom url"
            value={custom_url}
            placeholder="http://nbtiqrgesp.dockup.codemancers.com/"
            onChange={e =>
              handleTextFieldChange(name, e.target.value, "custom_url")
            }
          />
        ) : (
          ""
        )}
      </div>
      <FormControl required={true}>
        <InputLabel htmlFor="dependencies">Enter Dependences</InputLabel>
        <ChipInput
          //classes={styles.card}
          newChipKeyCodes={[13]}
          allowDuplicates={false}
          onChange={e =>
            handleTextFieldChange(name, e.target.value, "dependencies")
          }
          value={dependencies}
          onAdd={value => handleAddChip(name, value)}
          onDelete={(chip, index) => {
            handleRemoveDependency(name, chip, index);
            handleDeleteChip(name, chip, index);
          }}
        />
      </FormControl>
      <FormControl required={true}>
        <InputLabel htmlFor="Command">Command</InputLabel>
        <Input
          type="text"
          name="command"
          value={command}
          onChange={e => handleTextFieldChange(name, e.target.value, "command")}
        />
      </FormControl>
    </Card>
  );
};

export default withStyles(styles)(ContainerForm);

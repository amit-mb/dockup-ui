import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Forward from "@material-ui/icons/Forward";
import Check from "@material-ui/icons/Check";
import Button from "@material-ui/core/Button";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
});

const Buttons = props => (
  <div id="btn-container">
    <Button
      variant="contained"
      color="primary"
      className={props.classes.button}
      onClick={() => {
        props.stepChange(props.step + 1, props.containers);
      }}
    >
      Next
      <Forward className={props.classes.rightIcon} />
    </Button>
    <Button
      variant="contained"
      color="primary"
      className={props.classes.button}
      onClick={props.handleSubmit}
    >
      Submit
      <Check className={props.classes.rightIcon} />
    </Button>
  </div>
);

Buttons.defaultProps = {
  step: 0,
};

export default withStyles(styles)(Buttons);

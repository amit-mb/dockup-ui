import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import TextField from "@material-ui/core/Input";
import AddIcon from "@material-ui/icons/Add";
import Grid from "@material-ui/core/Grid";
import { CardContent, Icon, Checkbox } from "@material-ui/core";

const styles = {
  card: {
    width: 300,
    margin: "200px auto"
  },
  heading: {
    textAlign: "center"
  },
  textfield: {
    margin: "20px 40px"
  },
  root: {
    flexGrow: 1
  },
  flex: {
    flex: 1
  }
};

const showContainers = (props, classes) =>
  props.containerNames.map((name, index) => (
    <TextField
      id={`containerNames${index}`}
      key={index}
      value={name}
      placeholder="Enter Container  Name"
      className={classes.textfield}
      disabled={props.initContainer && index === 0 ? true : false}
      error={!name.length}
      onChange={e => {
        props.updateContainerNames(index, e.target.value);
      }}
    />
  ));

const Projects = props => {
  const { classes } = props;
  const handleSubmitProjectName = () => {
    if (!props.projectName.length || containerNamesValidate()) {
      props.showErrorMsg("Enter proper Container names or project Names");
    } else {
      props.handleSubmitProjectName();
    }
  };
  const containerNamesValidate = () => {
    let count = 0;
    props.containerNames.forEach(name => {
      if (!name.length) {
        count++;
      }
    });
    return !count == 0;
  };
  return (
    <div>
      <Card className={classes.card}>
        <CardContent>
          <Typography
            variant="headline"
            component="h2"
            className={classes.heading}
          >
            Create Project
          </Typography>
          {props.errorMsg}
          <TextField
            id="projectName"
            value={props.projectName}
            label="Projects"
            placeholder="Enter Project Name"
            className={classes.textfield}
            error={!props.projectName.length}
            onChange={props.handleProjectNameChange}
          />
          <Grid container spacing={0}>
            <Grid item xs={2}>
              <Checkbox
                checked={props.initContainer}
                onChange={props.updateInitContainer}
              />
            </Grid>
            <Grid item xs={10}>
              <Typography>Init Container</Typography>
            </Grid>
            {showContainers(props, classes)}
            <Button
              variant="fab"
              color="primary"
              aria-label="add"
              className={classes.button}
              onClick={props.handleAddContainerButton}
            >
              <AddIcon />
            </Button>
          </Grid>
          <Button
            onClick={() => {
              handleSubmitProjectName();
            }}
          >
            Submit
          </Button>
        </CardContent>
      </Card>
    </div>
  );
};

export default withStyles(styles)(Projects);

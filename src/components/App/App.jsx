import React, {Component, Fragment} from "react";
import {Provider} from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";
import Header from "./Header";
import FormsWrapperHOC from "./FormsWrapperHOC";
import store from "./../../store";

class App extends Component {
  render() {
    return (
      <div>
        <CssBaseline />
        <Header />
        <Provider store={store}>
          <FormsWrapperHOC />
        </Provider>
      </div>
    );
  }
}

export default App;

import React, { Fragment } from "react";
import ContainerForm from "./Container";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { StepperWizard } from "./StepperComponent";
import { withStyles } from "@material-ui/core/styles";
import Buttons from "./Buttons";
import Projects from "./Projects";

import {
  handleRemoveDependency,
  handleAddChip,
  handleDeleteChip,
  handleSubmit,
  stepChange,
  toggleCheckCustomUrl,
  handleTextFieldChange,
  handleTagChange,
  handleProjectNameChange,
  handleSubmitProjectName,
  updateContainerNames,
  handleAddContainerButton,
  updateInitContainer,
  showErrorMsg
} from "./../../store/actionCreators";

const styles = {
  root: {
    margin: "auto 50px"
  }
};

class FormsWrapperHOC extends React.Component {
  constructor() {
    super();
  }
  render() {
    const { props } = this;
    return (
      <div>
        <Fragment>
          <StepperWizard step={props.step} />
          {this.showComponent()}
          <Buttons
            handleSubmit={props.handleSubmit}
            stepChange={props.stepChange}
            step={props.step}
            containers={props.containerNames}
          />
        </Fragment>
      </div>
    );
  }
  showComponent = () => {
    const { props } = this;
    switch (props.step) {
      case 0:
        return (
          <Projects
            projectName={props.projectName}
            handleProjectNameChange={props.handleProjectNameChange}
            handleSubmitProjectName={props.handleSubmitProjectName}
            containerNames={props.containerNames}
            updateContainerNames={props.updateContainerNames}
            handleAddContainerButton={props.handleAddContainerButton}
            initContainer={props.initContainer}
            updateInitContainer={props.updateInitContainer}
            showErrorMsg={props.showErrorMsg}
            errorMsg={props.errorMsg}
          />
        );
      case 1:
        return (
          <Grid container spacing={24}>
            {this.AllContainers()}
          </Grid>
        );
    }
  };
  AllContainers = () => {
    const { props } = this;
    return Object.entries(props.forms).map((form, index) => {
      const [formName, formObject] = form;
      const {
        image,
        env_variables,
        command,
        dependencies,
        tag,
        custom_url_check,
        custom_url,
        ports
      } = formObject;
      return (
        <Grid item xs={12} lg={3} sm={6} key={index}>
          <ContainerForm
            className={props.classes.root}
            name={formName}
            tag={tag}
            custom_url={custom_url}
            custom_url_check={custom_url_check}
            ports={ports}
            image={image}
            command={command}
            dependencies={dependencies}
            env_variables={env_variables}
            handleRemoveDependency={props.handleRemoveDependency}
            handleDeleteChip={props.handleDeleteChip}
            handleAddChip={props.handleAddChip}
            toggleCheckCustomUrl={props.toggleCheckCustomUrl}
            handleTextFieldChange={props.handleTextFieldChange}
          />
        </Grid>
      );
    });
  };
}

const mapStateToProps = state => ({
  ...state
});

const mapDispatchToProps = {
  handleRemoveDependency,
  handleAddChip,
  handleDeleteChip,
  handleSubmit,
  stepChange,
  toggleCheckCustomUrl,
  handleTextFieldChange,
  handleTagChange,
  handleProjectNameChange,
  handleSubmitProjectName,
  updateContainerNames,
  handleAddContainerButton,
  updateInitContainer,
  showErrorMsg
};
export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(FormsWrapperHOC)
);

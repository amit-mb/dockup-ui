import {
  ADD_DEPENDENCY,
  ADD_CONTAINERS,
  DELETE_CHIP,
  DELETE_DEPENDENCY,
  FORM_SUBMITTED,
  STEP_CHANGE,
  TOGGLE_CUSTOMURL_CHECK,
  HANDLE_TEXTFIELD_CHANGE,
  HANDLE_PROJECT_NAME_CHANGE,
  HANDLE_PROJECT_NAME_SUBMIT_BUTTON_CLICK,
  UPDATE_CONTAINER_NAMES,
  TOGGLE_INIT_CONTAINER,
  REMOVE_INIT_CONTAINER,
  ADD_INIT_CONTAINER,
  ERROR_MSG
} from "../store/actionCreators";
const initialState = {
  step: 0,
  forms: {},
  projectName: "",
  containerNames: [],
  initContainer: false,
  errorMsg: ""
};
const initialFormState = {
  image: "",
  tag: "",
  ports: "",
  env_variables: "",
  command: "",
  dependencies: [],
  custom_url: "",
  custom_url_check: false
};
const extractPortNumbers = state => {
  let ports;
  Object.values(state.forms).map((containerData, index) => {
    ports = containerData.ports.split(",");
    ports = ports.map((eachPortNo, index) => eachPortNo.trim());
  });
  return stateWithExtractedPorts(state, ports);
};

const stateWithExtractedPorts = (state, portsArray) => {
  return Object.keys(state.forms).map((formName, index) => {
    return {
      [formName]: {
        ...state.forms[formName],
        ports: portsArray
      }
    };
  });
};

const addDependency = (state, payload, props) => ({
  ...state,
  forms: {
    ...state.forms,
    [payload.name]: {
      ...state.forms[payload.name],
      ...props
    },
    [payload.value]: initialFormState
  }
});

const deleteDependency = (state, payload) => {
  const newForms = removeForm(state.forms, payload.value);
  return {
    ...state,
    forms: newForms
  };
};

const removeTag = (state, payload) => {
  const removedDependency = removeDependency(
    state.forms[payload.name].dependencies,
    payload.value
  );
  return {
    ...state,
    forms: {
      ...state.forms,
      [payload.name]: {
        ...state.forms[payload.name],
        dependencies: removedDependency
      }
    }
  };
};

const removeDependency = (dependencyList, value) => {
  return dependencyList.filter(item => item !== value);
};

const removeForm = (forms, name) => {
  let res = Object.assign({}, forms);
  delete res[name];
  return res;
};

const extendForm = (state, payload, props) => ({
  ...state,
  forms: {
    ...state.forms,
    [payload.name]: {
      ...state.forms[payload.name],
      ...props
    }
  }
});

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_DEPENDENCY: {
      return addDependency(state, action.payload, {
        dependencies: [
          ...state.forms[action.payload.name].dependencies,
          action.payload.value
        ]
      });
    }
    case DELETE_CHIP: {
      return {
        ...removeTag(state, action.payload)
      };
    }
    case DELETE_DEPENDENCY: {
      return { ...deleteDependency(state, action.payload) };
    }
    case STEP_CHANGE: {
      return {
        ...state,
        step: action.step,
        forms: action.forms
      };
    }
    case TOGGLE_CUSTOMURL_CHECK: {
      return extendForm(state, action.payload, {
        custom_url_check: action.payload.checked
      });
    }
    case HANDLE_TEXTFIELD_CHANGE: {
      return extendForm(state, action.payload, {
        [action.payload.field]: action.payload.value
      });
    }

    case FORM_SUBMITTED: {
      console.log(extractPortNumbers(state));
    }
    case HANDLE_PROJECT_NAME_CHANGE: {
      return {
        ...state,
        projectName: action.payload.projectName
      };
    }
    case HANDLE_PROJECT_NAME_SUBMIT_BUTTON_CLICK:
      let forms = state.forms;
      for (let i in state.containerNames) {
        forms = Object.assign({}, forms, {
          [state.containerNames[i]]: initialFormState
        });
      }
      return {
        ...state,
        forms: forms,
        step: 1,
        validateCreateProject: ""
      };

    case UPDATE_CONTAINER_NAMES:
      return Object.assign({}, state, {
        containerNames: [
          ...state.containerNames.slice(0, action.payload.index),
          action.payload.value,
          ...state.containerNames.slice(action.payload.index + 1)
        ]
      });

    case ADD_CONTAINERS:
      return {
        ...state,
        containerNames: state.containerNames.concat([""])
      };
    case TOGGLE_INIT_CONTAINER:
      return {
        ...state,
        initContainer: !state.initContainer
      };
    case REMOVE_INIT_CONTAINER:
      return {
        ...state,
        containerNames: [...state.containerNames.slice(1)]
      };
    case ADD_INIT_CONTAINER:
      return {
        ...state,
        containerNames: ["init", ...state.containerNames.slice(0)]
      };
    case ERROR_MSG:
      return {
        ...state,
        errorMsg: action.payload.errorMsg
      };
    default:
      return state;
  }
};
